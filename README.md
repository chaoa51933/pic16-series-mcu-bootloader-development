# PIC16-Series-MCU-Bootloader-Development

#### 简介:
Microchip PIC16系列MCU Bootloader开发例程分享，下载例程到本地后，大家可以用Google浏览器打开相关项目文件夹下的readme.html，内有详细的工程建立和验证过程。

#### 参考文档:
1) [公众号文章-PIC MCU Bootloader开发基础](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&__biz=MzAxODgwMjkyOA==&scene=1&album_id=2116596053905899521&count=3&uin=MjU5ODU0MjYyMQ%3D%3D&key=8670448a25b6794c62ce1bcfe1352ee4109d1203830364a76c007e8473df792c147fc66fa066bf4a96ca15742617459727b8fde63c7e4ec7af03039b6b4b2dd680e1e97991b82fb849c12da1c89cda46f8fd108a5bd0dda78db4c3ffdda31f40aa88a6e11407c95e98f13f87b765fcf9b24038b42301bd2598e67db9b48bfab3&devicetype=Windows+10&version=62060833&lang=en&ascene=1&pass_ticket=TeVtLWj0cGGiXuN%2BEjKpj4GlBmA98XPreYEvslulQEXsVx49Rbsq9Zjxfs%2BrCfBy)
***
欢迎同步关注微信公众号，促使我进步:-)  
![image](./images/wechat.png)
***

#### 工程文件夹结构:
工程文件夹结构如下图所示，包含4个文件夹和一个readme.html。其中app_picxxx目录为应用程序工程，配合Bootloader工程使用；boot_picxxx目录为Bootloader工程；images目录为开发过程图片记录；scripts目录为python相关脚本，实现应用程序和Bootloader的Hex合并以及串口升级。
![image](./images/example_project_format.png) 

#### Hex文件合并操作:
应用程序和Bootloader的Hex合并依赖于hex_merge.py文件，可通过设置hex_merge_app_booot.bat批处理文件来获得合并hex文件所需的相关参数，合并过程示意如下：
![image](./images/combine_hex.png) 

#### 串口升级过程操作:
串口升级过程依赖于pic16_uploader.py文件，可通过设置serial_uplooad.bat批处理文件来获得升级过程所需的相关参数，升级过程示意如下：
![image](./images/serial_boot.png)
